from collections import deque
from graphe import Graphe
from arbre import Arbre


# noinspection DuplicatedCode
class CalculeChemin(object):
    def __init__(self, graphe: Graphe, depart: str):
        self.graphe: Graphe = graphe
        self.depart: str = depart

    def __heuristique(self, arrivee: Arbre) -> float:
        return self.graphe[arrivee.nom].heuristique

    def __cout(self, depart: Arbre, arrivee: Arbre) -> float:
        return self.graphe[depart.nom].voisins[arrivee.nom]

    def best_first_search(self) -> Arbre or None:
        ferme: list[Arbre] = []
        ouvert: deque[Arbre] = deque()
        ouvert.append(Arbre(self.depart, None))

        # print("BFS")
        while len(ouvert):
            # print(ouvert)
            # print(ferme)
            n = ouvert.popleft()
            ferme.append(n)

            for str_nprime in self.graphe.get_voisins(n.nom):
                nprime = Arbre(str_nprime, n)
                n.enfants.append(nprime)
                if self.graphe[str_nprime].heuristique == 0:  # C'est la solution
                    return nprime

                # Calcul de f
                nprime.heuristique = self.__heuristique(nprime)
                nprime.total = nprime.heuristique

                # Verification de presence
                dans_ouvert = [x for x in ouvert if x.nom == nprime.nom]
                dans_ferme = [x for x in ferme if x.nom == nprime.nom]
                if not dans_ouvert and not dans_ferme:
                    ouvert.append(nprime)
                else:
                    # Mise a jour du cout
                    old = dans_ouvert[0] if dans_ouvert else dans_ferme[0]
                    if old.heuristique <= nprime.heuristique:
                        n.enfants.pop()
                    else:
                        ouvert.remove(old) if dans_ouvert else ferme.remove(old)
                        ouvert.append(nprime)

                # Tri
                temp: list[Arbre] = list(ouvert)
                temp.sort()
                ouvert = deque(temp)
        # On a rien trouve
        return None

    def branch_and_bound(self) -> Arbre or None:
        ferme: list[Arbre] = []
        ouvert: deque[Arbre] = deque()
        ouvert.append(Arbre(self.depart, None))

        # print("BNB")
        while len(ouvert):
            # print(ouvert)
            # print(ferme)
            n = ouvert.popleft()
            ferme.append(n)

            for str_nprime in self.graphe.get_voisins(n.nom):
                nprime = Arbre(str_nprime, n)
                n.enfants.append(nprime)
                if self.graphe[str_nprime].heuristique == 0:  # C'est la solution
                    return nprime

                # Calcul de f
                nprime.cout = self.__cout(n, nprime) + n.cout
                nprime.total = nprime.cout

                # Verification de presence
                dans_ouvert = [x for x in ouvert if x.nom == nprime.nom]
                dans_ferme = [x for x in ferme if x.nom == nprime.nom]
                if not dans_ouvert and not dans_ferme:
                    ouvert.append(nprime)
                else:
                    # Mise a jour du cout
                    old = dans_ouvert[0] if dans_ouvert else dans_ferme[0]
                    if old.cout <= nprime.cout:
                        n.enfants.pop()
                    else:
                        ouvert.remove(old) if dans_ouvert else ferme.remove(old)
                        ouvert.append(nprime)

                # Tri
                temp: list[Arbre] = list(ouvert)
                temp.sort()
                ouvert = deque(temp)
        # On a rien trouve
        return None

    def astar(self) -> Arbre or None:
        ferme: list[Arbre] = []
        ouvert: deque[Arbre] = deque()
        ouvert.append(Arbre(self.depart, None))

        # print("A*")
        while len(ouvert):
            # print(ouvert)
            # print(ferme)
            n = ouvert.popleft()
            ferme.append(n)

            for str_nprime in self.graphe.get_voisins(n.nom):
                nprime = Arbre(str_nprime, n)
                n.enfants.append(nprime)
                if self.graphe[str_nprime].heuristique == 0:  # C'est la solution
                    return nprime

                # Calcul de f
                nprime.cout = self.__cout(n, nprime) + n.cout
                nprime.heuristique = self.__heuristique(nprime)
                nprime.total = nprime.cout + nprime.heuristique

                # Verification de presence
                dans_ouvert = [x for x in ouvert if x.nom == nprime.nom]
                dans_ferme = [x for x in ferme if x.nom == nprime.nom]
                if not dans_ouvert and not dans_ferme:
                    ouvert.append(nprime)
                else:
                    # Mise a jour du cout
                    old = dans_ouvert[0] if dans_ouvert else dans_ferme[0]
                    if old.heuristique <= nprime.heuristique:
                        n.enfants.pop()
                    else:
                        ouvert.remove(old) if dans_ouvert else ferme.remove(old)
                        ouvert.append(nprime)

                # Tri
                temp: list[Arbre] = list(ouvert)
                temp.sort()
                ouvert = deque(temp)
        # On a rien trouve
        return None
