class NoeudGraphe(object):
    def __init__(self, nom: str, heuristique: float, voisins: dict[str, int]):
        self.nom = nom
        self.voisins = voisins
        self.heuristique = heuristique

    def __repr__(self):
        return "({0} {1})".format(self.nom, self.heuristique)


class Graphe(object):
    def __init__(self, graphe: dict[str, NoeudGraphe]):
        self.graphe: dict[str, NoeudGraphe] = graphe

    def __getitem__(self, node: str) -> NoeudGraphe:
        return self.graphe[node]

    def __repr__(self):
        res: str = ""
        for key, value in self.graphe.items():
            res += key + " - " + str(value) + "\n"
        return res

    def get_voisins(self, node: str) -> dict[str, int]:
        try:
            return self.graphe[node].voisins
        except KeyError:
            return {}
