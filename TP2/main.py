from graphe import Graphe, NoeudGraphe
from calculechemin import CalculeChemin
from hanoi import Hanoi

try:
    choix = 0
    while choix != 1 and choix != 2:
        choix = int(input("Numero de l'exercice desire : "))

    if choix == 1:
        Hanoi(int(input("Nombre de disques ? : ")), int(input("Temps de pause (0.1 - 5) ? : ")))
    else:
        graphe = Graphe({
            "S": NoeudGraphe("S", 5, {"A": 2, "C": 3}),
            "A": NoeudGraphe("A", 2, {"B": 1, "E": 8}),
            "B": NoeudGraphe("B", 1, {"G1": 4, "D": 1}),
            "C": NoeudGraphe("C", 3, {"D": 1, "G2": 5}),
            "D": NoeudGraphe("D", 1, {"G1": 5, "G2": 1}),
            "E": NoeudGraphe("E", 6, {"G1": 9, "G2": 7}),
            "G1": NoeudGraphe("G1", 0, {"S": 4}),
            "G2": NoeudGraphe("G2", 0, {})
        })
        calculeChemin = CalculeChemin(graphe, "S")
        bfs = calculeChemin.best_first_search()
        bnb = calculeChemin.branch_and_bound()
        astar = calculeChemin.astar()

        try:
            print("La solution pour Best First Search est :", bfs.get_parents() + ".")
        except AttributeError:
            print("Aucune solution n'a ete trouvee pour A*.")
        try:
            print("La solution pour Branch And Bound est :", bnb.get_parents() + ".")
        except AttributeError:
            print("Aucune solution n'a ete trouvee pour A*.")
        try:
            print("La solution pour A* est :", astar.get_parents() + ".")
        except AttributeError:
            print("Aucune solution n'a ete trouvee pour A*.")

except ValueError:
    print("Valeur invalide.")
    exit(1)
