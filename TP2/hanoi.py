from tkinter import Tk, Canvas, simpledialog
from time import sleep
from typing import Callable


class Hanoi:
    def __init__(self, n: int, temps_pause: float = 0.5):
        self.n: int = n if n > 0 else 3
        self.temps_pause: float = temps_pause if 0 < temps_pause <= 5 else 0.5
        self.tk: Tk = Tk()
        self.canvas: Canvas = Canvas(self.tk)
        self.tubes: list[Canvas] = []  # Les tubes de l'ecran
        self.etat_tube: list[list[int]] = [[], [], []]  # L'etat de chaque tube
        self.pieces: dict[int, Canvas] = {}  # L'etat des pieces
        self.running: bool = False  # L'etat d'execution

        # Parametrage de la fenetre
        self.tk.title("Tours de Hanoi n=" + str(n))
        self.canvas.configure(bg="lightgrey")
        self.canvas.pack()
        self.__init_interface()
        self.tk.bind("<space>", self.__run)
        self.tk.bind("r", self.__reset)
        self.tk.bind("t", self.__set_temps_pause)
        # Affichage de la fenetre au premier plan
        self.tk.lift()
        self.tk.attributes("-topmost", True)
        self.tk.after_idle(self.tk.attributes, "-topmost", False)
        self.tk.mainloop()

    def __init_interface(self) -> None:
        largeur, hauteur = self.tk.getint(self.canvas["width"]), self.tk.getint(self.canvas["height"])
        self.canvas.create_text(100, 25, text="Demarrer : Espace\nReset : R\nChangement intervalle de temps : T")

        # Taille des tubes
        largeur_tube = 10
        hauteur_tube = hauteur // 2
        distance_tube = largeur // 3
        x1, y1 = (distance_tube - largeur_tube) // 2, hauteur // 3
        x2, y2 = x1 + largeur_tube, y1 + hauteur_tube
        # Generation des tubes
        self.canvas.create_rectangle(0, y2, largeur, y2 + largeur_tube, fill="black")
        for i in range(3):
            tube = self.canvas.create_rectangle(x1, y1, x2, y2, fill="black")
            self.tubes.append(tube)
            x1, x2 = x1 + distance_tube, x2 + distance_tube

        # Taille des pieces
        hauteur_piece = hauteur_tube // 15
        largeur_piece_min = 2 * largeur_tube
        largeur_piece_max = distance_tube * 2 // 3
        dx = (largeur_piece_max - largeur_piece_min) // (2 * max(1, self.n - 1))
        x1, y1 = (distance_tube - largeur_piece_max) // 2, y2 - hauteur_piece - 2
        x2, y2 = x1 + largeur_piece_max, y1 + hauteur_piece
        # Generation des pieces
        for i in range(self.n, 0, -1):
            piece = self.canvas.create_rectangle(x1, y1, x2, y2, fill="dodger blue")
            self.pieces[i] = piece
            self.etat_tube[0].append(i)
            x1, x2 = x1 + dx, x2 - dx
            y1, y2 = y1 - hauteur_piece - 2, y2 - hauteur_piece - 2

    def __hanoi(self, n: int, source: int, destination: int, auxiliaire: int, actualiseur: Callable[[int, int, int], None]) -> None:
        if n <= 0:
            return
        self.__hanoi(n - 1, source, auxiliaire, destination, actualiseur)
        actualiseur(n, source, destination)
        self.__hanoi(n - 1, auxiliaire, destination, source, actualiseur)

    def __run(self, event) -> None:
        if not self.running and len(self.etat_tube[2]) == 0:
            self.running = True
            self.__hanoi(self.n, 0, 2, 1, self.__deplacer_piece)
            self.running = False

    def __reset(self, event) -> None:
        if not self.running:
            self.canvas.delete("all")
            self.tubes = []
            self.etat_tube = [[], [], []]
            self.pieces = {}
            self.__init_interface()

    def __set_temps_pause(self, event) -> None:
        reponse = simpledialog.askfloat("Saisie", "Saisir votre temps de pause entre deux mouvements (Entre 0.1 et 5) : ",
                                        parent=self.tk, minvalue=0.1, maxvalue=5)
        if reponse is not None:
            self.temps_pause = reponse

    def __deplacer_piece(self, i: int, source: int, destination: int) -> None:
        del self.etat_tube[source][-1]

        # Recuperation des coordonnes du tube d'arrivee
        t_x1, t_y1, t_x2, t_y2 = self.canvas.bbox(self.tubes[destination])
        centre_arrivee = (t_x1 + t_x2) // 2

        # Recuperation des coordonnes de la piece active
        p_x1, p_y1, p_x2, p_y2 = self.canvas.bbox(self.pieces[i])
        centre_piece = (p_x1 + p_x2) // 2

        # Calcul de la hauteur de la piece
        hauteur_piece = p_y2 - p_y1
        bas_arrivee = t_y2 - hauteur_piece * len(self.etat_tube[destination]) - 2

        # Deplacement de la piece
        self.canvas.move(self.pieces[i], centre_arrivee - centre_piece, bas_arrivee - p_y2)
        self.etat_tube[destination].append(i)

        # Mise a jour de l'affichage
        self.tk.update()
        sleep(self.temps_pause)
