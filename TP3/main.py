from arbre import Arbre
from meilleurcout import alpha_beta, minimax
import copy

racine_alpha_beta = \
    Arbre(0, [
        Arbre(0, [
            Arbre(0, [
                Arbre(0, [Arbre(4), Arbre(3), Arbre(8)]),
                Arbre(0, [Arbre(2), Arbre(1)])
            ]),
            Arbre(0, [
                Arbre(0, [Arbre(4), Arbre(2), Arbre(3)])
            ]),
            Arbre(0, [
                Arbre(0, [Arbre(6), Arbre(4)]),
                Arbre(0, [Arbre(7)]),
                Arbre(0, [Arbre(5), Arbre(2)])
            ])
        ]),
        Arbre(0, [
            Arbre(0, [
                Arbre(0, [Arbre(1), Arbre(9), Arbre(0)])
            ]),
            Arbre(0, [
                Arbre(0, [Arbre(4), Arbre(3)]),
                Arbre(0, [Arbre(0)])
            ]),
            Arbre(0, [
                Arbre(0, [Arbre(2), Arbre(8), Arbre(4)]),
                Arbre(0, [Arbre(3), Arbre(7)]),
                Arbre(0, [Arbre(5), Arbre(4), Arbre(1)])
            ])
        ])
    ])

racine_minmax = copy.deepcopy(racine_alpha_beta)

eval_minimax = minimax(racine_minmax, racine_minmax.get_profondeur(), True)
eval_alpha_beta = alpha_beta(racine_alpha_beta, racine_alpha_beta.get_profondeur(), racine_alpha_beta.alpha, racine_alpha_beta.beta, True)
print("La solution de Minimax est :", eval_minimax)
print("La solution de Alpha-Beta est :", eval_alpha_beta)
