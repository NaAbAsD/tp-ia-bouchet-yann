from arbre import Arbre


def minimax(position: Arbre, profondeur: int, maximiser: bool) -> float:
    if profondeur == 0 or position.valeur == float("+inf") or position.valeur == float("-inf"):
        return position.valeur
    if maximiser:
        max_eval = float("-inf")
        for enfant in position.enfants:
            eval_ = minimax(enfant, profondeur - 1, False)
            max_eval = max(max_eval, eval_)
        return max_eval
    else:
        min_eval = float("+inf")
        for enfant in position.enfants:
            eval_ = minimax(enfant, profondeur - 1, True)
            min_eval = min(min_eval, eval_)
        return min_eval


def alpha_beta(position: "Arbre", profondeur: int, alpha: float, beta: float, maximiser: bool) -> float:
    if profondeur == 0 or position.valeur == float("+inf") or position.valeur == float("-inf"):
        return position.valeur
    if maximiser:
        max_eval = float("-inf")
        for enfant in position.enfants:
            eval_ = alpha_beta(enfant, profondeur - 1, alpha, beta, False)
            max_eval = max(max_eval, eval_)
            alpha = max(alpha, eval_)
            position.alpha = alpha
            position.beta = beta
            if beta <= alpha:
                break
        position.valeur = max_eval
        return max_eval
    else:
        min_eval = float("+inf")
        for enfant in position.enfants:
            eval_ = alpha_beta(enfant, profondeur - 1, alpha, beta, True)
            min_eval = min(min_eval, eval_)
            beta = min(beta, eval_)
            position.alpha = alpha
            position.beta = beta
            if beta <= alpha:
                break
        position.valeur = min_eval
        return min_eval
