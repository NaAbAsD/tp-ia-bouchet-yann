class Arbre(object):
    def __init__(self, valeur: float, enfants: float or list["Arbre"] = None):
        self.valeur: float = valeur
        self.parent: "Arbre" or None = None
        self.enfants: list["Arbre"] = []
        self.alpha: float = float("-inf")
        self.beta: float = float("+inf")
        if enfants is not None:
            for enfant in enfants:
                self.ajouter_enfant(enfant)

    def __lt__(self, other: "Arbre"):
        return self.valeur < other.valeur

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "valeur = {0} (alpha = {1} / beta = {2})".format(self.valeur, self.alpha, self.beta)

    def ajouter_enfant(self, node: "Arbre") -> None:
        node.parent = self
        self.enfants.append(node)

    def get_profondeur(self) -> int:
        return 1 + self.enfants[0].get_profondeur() if len(self.enfants) else 0

    def get_parents(self) -> str:
        res: str = str(self.valeur)
        obj: "Arbre" = self
        while obj.parent is not None:
            res += obj.parent.valeur if len(obj.parent.valeur) < 2 else obj.parent.valeur[::-1]
            obj = obj.parent
        return res[::-1]
