class Arbre(object):
    def __init__(self, nom: str, parent: "Arbre" or None):
        self.nom: str = nom
        self.parent: "Arbre" or None = parent
        self.enfants: list["Arbre"] = []
        self.cout: float = 0  # Fonction g
        self.heuristique: float = 0  # Fonction h
        self.total: float = 0  # Fonction f

    def __lt__(self, other: "Arbre"):
        return self.total < other.total

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.nom

    def get_parents(self) -> str:
        res: str = self.nom if len(self.nom) < 2 else self.nom[::-1]  # Pour inverser les noms comme G1 ou G2
        obj: "Arbre" = self
        while obj.parent is not None:
            res += obj.parent.nom if len(obj.parent.nom) < 2 else obj.parent.nom[::-1]
            obj = obj.parent
        return res[::-1]
