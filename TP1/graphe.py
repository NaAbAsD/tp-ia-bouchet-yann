class NoeudGraphe(object):
    def __init__(self, nom: str, position: tuple[int, int, int], voisins: list[str]):
        self.nom: str = nom
        self.x, self.y, self.z = position
        self.voisins: list[str] = voisins

    def __repr__(self):
        return "({0} {1} {2})".format(self.x, self.y, self.z)


class Graphe(object):
    def __init__(self, graphe: dict[str, NoeudGraphe]):
        self.graphe: dict[str, NoeudGraphe] = graphe

    def __getitem__(self, node: str) -> NoeudGraphe:
        return self.graphe[node]

    def __repr__(self):
        res: str = ""
        for key, value in self.graphe.items():
            res += key + " - " + str(value) + "\n"
        return res

    def get_voisins(self, node: str) -> list[str]:
        try:
            return self.graphe[node].voisins
        except KeyError:
            return []
