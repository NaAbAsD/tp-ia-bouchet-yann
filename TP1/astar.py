from collections import deque
from math import sqrt
from graphe import Graphe
from arbre import Arbre


class Astar(object):
    def __init__(self, graphe: Graphe, depart: str, arrivee: str):
        self.graphe: Graphe = graphe
        self.depart: str = depart
        self.arrivee: str = arrivee

    def __heuristique(self, depart: Arbre) -> float:
        spos = self.graphe[depart.nom]
        fpos = self.graphe[self.arrivee]
        return sqrt((fpos.x - spos.x) ** 2 + (fpos.y - spos.y) ** 2 + (fpos.z - spos.z) ** 2)

    def __cout(self, depart: Arbre, arrivee: Arbre) -> float:
        spos = self.graphe[depart.nom]
        fpos = self.graphe[arrivee.nom]
        return abs(spos.x - fpos.x) + abs(spos.y - fpos.y) + abs(spos.z - fpos.z)

    def astar(self) -> Arbre or None:
        ferme: list[Arbre] = []
        ouvert: deque[Arbre] = deque()
        ouvert.append(Arbre(self.depart, None))

        # print("A*")
        while len(ouvert):
            # print(ouvert)
            # print(ferme)
            n = ouvert.popleft()
            ferme.append(n)

            for str_nprime in self.graphe.get_voisins(n.nom):
                nprime = Arbre(str_nprime, n)
                n.enfants.append(nprime)
                if nprime.nom == self.arrivee:  # C'est la solution
                    return nprime

                # Calcul de f
                nprime.cout = self.__cout(n, nprime) + n.cout
                nprime.heuristique = self.__heuristique(nprime)
                nprime.total = nprime.cout + nprime.heuristique

                # Verification de presence
                dans_ouvert = [x for x in ouvert if x.nom == nprime.nom]
                dans_ferme = [x for x in ferme if x.nom == nprime.nom]
                if not dans_ouvert and not dans_ferme:
                    ouvert.append(nprime)
                else:
                    # Mise a jour du cout
                    old = dans_ouvert[0] if dans_ouvert else dans_ferme[0]
                    if old.heuristique <= nprime.heuristique:
                        n.enfants.pop()
                    else:
                        ouvert.remove(old) if dans_ouvert else ferme.remove(old)
                        ouvert.append(nprime)

                # Tri
                temp: list[Arbre] = list(ouvert)
                temp.sort()
                ouvert = deque(temp)
        # On a rien trouve
        return None
