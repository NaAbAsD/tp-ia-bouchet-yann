from graphe import Graphe, NoeudGraphe
from astar import Astar

graphe = Graphe({
    "A": NoeudGraphe("A", (0, 0, 0), ["B", "D"]),
    "B": NoeudGraphe("B", (0, 1, 0), ["A", "C"]),
    "C": NoeudGraphe("C", (1, 1, 0), ["B", "F"]),
    "D": NoeudGraphe("D", (2, 0, 0), ["A", "E"]),
    "E": NoeudGraphe("E", (2, 2, 0), ["D", "F", "G"]),
    "F": NoeudGraphe("F", (1, 2, 0), ["C", "E"]),
    "G": NoeudGraphe("G", (2, 2, 2), ["E", "H"]),
    "H": NoeudGraphe("H", (0, 2, 2), ["G"])
})
astar = Astar(graphe, "A", "H")
solution = astar.astar()

try:
    print("La solution est :", solution.get_parents() + ".")
except AttributeError:
    print("Aucune solution n'a ete trouvee.")
